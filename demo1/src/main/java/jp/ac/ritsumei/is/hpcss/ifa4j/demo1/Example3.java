/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.demo1;

import jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecyAnnotations.Secrecy;

@Secrecy(MySecrecy.H) class HighException extends Exception {}

@Secrecy(MySecrecy.L) public class Example3 {/*
    @Secrecy(H) String secretLog;
            Void v;

    @HeapEffect(H)
    @Secrecy(L) Void use() {
        try {
            v = method();
        } catch (HighException e) {
            secretLog = "";
        }

        if (lowResult == 1) {
            System.out.println("high is definily false; invalid information flow");
        }
        return null;
    }

    @Secrecy(H) boolean high;
    @Secrecy(L) boolean low;

    @Secrecy(L) int lowResult = 0;

    @HeapEffect(H)
    @Secrecy(L)
    Void method() throws HighException {
        if (low) {
            if (high) {
                HighException e = null;
                throw e;
            } else {

            }
            lowResult = 1;
        }
        return null;
    }*/
}
