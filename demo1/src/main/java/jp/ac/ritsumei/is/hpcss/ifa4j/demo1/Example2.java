/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.demo1;

import static jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecy.H;
import static jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecy.L;
import jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecyAnnotations.Secrecy;

@Secrecy(H) class MyException extends Exception {}

@Secrecy(L) class Echo {/*
    @Secrecy(L) String output;

    @HeapEffect(L) void echo() {
        @Secrecy(L) String input;
        @Secrecy(L) int len;
        @Secrecy(L) Void tmp;
        input = this.read();
        len = input.length();
        if (len == 0) {
            try {
                tmp = this.log("zero-length");
            } catch (MyException e) {
                tmp = this.print("i/o error");
            }
        } else {
            tmp = this.print(input);
        }
    }

    @HeapEffect(H)
    @Secrecy(L)
    Void log(String s) throws MyException {
        return null;
    }

    @HeapEffect(L)
    @Secrecy(L) String read() {
        return "";
    }

    @HeapEffect(L)
    @Secrecy(L)
    Void print(@Secrecy(L) String s) {
        this.output = s;
        return null;
    }*/
}
