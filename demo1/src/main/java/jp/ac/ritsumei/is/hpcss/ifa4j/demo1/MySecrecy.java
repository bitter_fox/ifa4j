/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.demo1;

import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyLattice;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.Top;
import jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecyAnnotations.LowerOf;

/**
 *
 * @author bitter_fox
 */
@SecrecyLattice
public enum MySecrecy {
    @Top H,
/*    @LowerOf(H) M1, @LowerOf(H) M2,
    @LowerOf(M2) M3,
    @LowerOf({M1, M3}) M4,
    @LowerOf(M4) L;*/
    @LowerOf(H) L;
}
