/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.demo1;
import static jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecy.H;
import static jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecy.L;

import jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecyAnnotations.HeapEffect;
import jp.ac.ritsumei.is.hpcss.ifa4j.demo1.MySecrecyAnnotations.Secrecy;

@Secrecy(L) class PasswordAuthentication {
    private @Secrecy(L) String user;
    private @Secrecy(H) String password;

    public @HeapEffect(L) @Secrecy(L) String
    getUser() {
        @Secrecy(L) String user;
        user = this.user;
        return user;
    }

    public @HeapEffect(L) @Secrecy(H) boolean
    comparePassword(@Secrecy(H) String target) {
        @Secrecy(H) String p;
        p = this.password;
        @Secrecy(H) boolean ok;
        ok = p.equals(target);
        return ok;
    }
}

@Secrecy(L) class LoginService {
    private @Secrecy(L) PasswordAuthentication auth;

    private @Secrecy(L) String loginUser;

    public @HeapEffect(L) @Secrecy(L) Void
    login(@Secrecy(H) String password) {
        @Secrecy(L) PasswordAuthentication auth;
        auth = this.auth;
        @Secrecy(H) boolean success;
        success = auth.comparePassword(password);
        if (success) {
            @Secrecy(L) String user;
            user = auth.getUser();
            this.loginUser = user;
        } else {
            this.loginUser = null;
        }
        return null;
    }
}
