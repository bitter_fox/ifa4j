/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import java.util.Objects;
import java.util.function.BiFunction;

/**
 *
 * @author bitter_fox
 */
public class SecrecyType {
    public final SecrecyLattice lattice;
    public final Symbol tsym;

    public SecrecyType(SecrecyLattice lattice, Symbol tsym) {
        this.lattice = lattice;
        this.tsym = tsym;
    }

    Name name() {
        return tsym.name;
    }

    @Override
    public String toString() {
        return "@Secrecy(" + tsym.getEnclosingElement().flatName() + "." + tsym.name + ")";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.lattice);
        hash = 67 * hash + Objects.hashCode(this.tsym);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SecrecyType other = (SecrecyType) obj;
        if (!Objects.equals(this.lattice, other.lattice)) {
            return false;
        }
        if (!Objects.equals(this.tsym, other.tsym)) {
            return false;
        }
        return true;
    }

    public boolean lowerThanOrEquals(SecrecyType that) {
        return thanOrEquals(that, SecrecyLattice::lowerBoundsOf);
    }

    public boolean upperThanOrEquals(SecrecyType that) {
        return thanOrEquals(that, SecrecyLattice::upperBoundsOf);
    }

    private boolean thanOrEquals(SecrecyType that, BiFunction<SecrecyLattice, SecrecyType, List<SecrecyType>> bounds) {
        return lattice.equals(that.lattice) &&
                bounds.apply(lattice, that).contains(this);
    }

    public boolean isAny() {
        return this.equals(lattice.any);
    }

    public SecrecyVariable instanceVariable(int pos) {
        return new SecrecyVariable(this, pos);
    }
}
