/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author bitter_fox
 */
class ConstraintSet {
    final Set<Constraint> constraints = new LinkedHashSet<>();
    final SecrecyLattice lattice;

    ConstraintSet(SecrecyLattice lattice) {
        this.lattice = lattice;
    }

    boolean add(Constraint c) {
        return constraints.add(c);
    }

    Stream<Constraint> stream() {
        return constraints.stream();
    }

    @Override
    public String toString() {
        return "ConstraintSet with " + lattice +
                constraints.stream()
                        .sorted(Comparator.comparingInt(c -> c.pos))
                        .map(Object::toString)
                        .collect(Collectors.joining(",\n", "{", "}"));
    }

    /**
     * 指定された機密度束に基づいて制約集合を解く。
     * 解は機密度の識別子と機密度値のマップ。
     * @param lattice 機密度束
     * @return 一つの解。解が複数存在する場合はその中のどれか一つ。
     *         解が見つからない場合は大きさ 0 のマップ。
     */
    public Map<String, SecrecyType> solve() {
        ConstraintSolver solver = new ConstraintSolver(lattice);
        solver.addConstraints(this);
        return solver.solve();
    }

    /**
     * 指定された機密度束に基づいて制約集合を解きすべての解を見つける。
     * 解は機密度の識別子と機密度値のマップ。
     * @param lattice 機密度束
     * @return 解のコレクション。解が見つからない場合は大きさ 0 のコレクション。
     */
    public Collection<Map<String, SecrecyType>> solveAll() {
        ConstraintSolver solver = new ConstraintSolver(lattice);
        solver.addConstraints(this);
        return solver.solveAll();
    }

    static class Constraint {
        final SecrecyVariable low;
        final SecrecyVariable high;
        final int pos;

        Constraint(SecrecyVariable low, SecrecyVariable high, int pos) {
            Objects.requireNonNull(low);
            Objects.requireNonNull(high);
            this.pos = pos;
            this.low = low;
            this.high = high;
        }

        @Override
        public String toString() {
            return ""+(low.type.isAny() ? "κ" : low.type.name())+low.id+"@"+low.pos +
                    " ⊑@"+pos + " " +
                    (high.type.isAny() ? "κ" : high.type.name())+high.id+"@"+high.pos+"";
        }
    }
}
