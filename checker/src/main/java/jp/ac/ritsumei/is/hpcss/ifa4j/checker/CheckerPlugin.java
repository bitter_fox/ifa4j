/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import com.sun.source.util.JavacTask;
import com.sun.source.util.Plugin;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskEvent.Kind;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.file.BaseFileObject;
import com.sun.tools.javac.file.JavacFileManager;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.util.BaseFileManager;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.JavacMessages;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Log;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ResourceBundle;

/**
 *
 * @author bitter_fox
 */
public class CheckerPlugin implements Plugin {

    @Override
    public String getName() {
        return "ifa";
    }

    public static void main(String[] args) {
    }

    @Override
    public void init(JavacTask jt, String... strings) {
        jt.addTaskListener(new TaskListener() {
            @Override
            public void started(TaskEvent te) {
            }

            @Override
            public void finished(TaskEvent te) {
                if (te.getKind() == Kind.ANALYZE) {
                    Context context = context(te);
                    Log.instance(context).useSource(te.getSourceFile());
//                        Thread.currentThread().setContextClassLoader(ClassLoader.getSystemClassLoader());
                    JCTree.JCCompilationUnit unit = (JCTree.JCCompilationUnit) te.getCompilationUnit();
                    unit.accept(SecrecyAttr.instance(context));
                }
            }
        });
    }

    Context context(TaskEvent te) {
        Context context = new Context();

        try {
            BaseFileObject sourceFile = (BaseFileObject) te.getSourceFile();
            Field fileManagerField = BaseFileObject.class.getDeclaredField("fileManager");
            fileManagerField.setAccessible(true);
            BaseFileManager fileManager = (BaseFileManager) fileManagerField.get(sourceFile);
            Field logField = BaseFileManager.class.getDeclaredField("log");
            logField.setAccessible(true);

            Log log = (Log) logField.get(fileManager);
            context.put(Log.logKey, log);

            Field messagesField = Log.class.getDeclaredField("messages");
            messagesField.setAccessible(true);

            JavacMessages javacMessages = (JavacMessages) messagesField.get(log);
            context.put(JavacMessages.messagesKey, javacMessages);

            ResourceBundle bundle = ResourceBundle
                    .getBundle("jp.ac.ritsumei.is.hpcss.ifa4j.checker.resources.compiler",
                            javacMessages.getCurrentLocale());
            List<ResourceBundle> bundles = javacMessages.getBundles(javacMessages.getCurrentLocale());
            Field currentBundles = javacMessages.getClass().getDeclaredField("currentBundles");
            currentBundles.setAccessible(true);
            currentBundles.set(javacMessages, bundles.append(bundle));
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }

        new JavacFileManager(context, true, Charset.defaultCharset());
        return context;
    }

}
