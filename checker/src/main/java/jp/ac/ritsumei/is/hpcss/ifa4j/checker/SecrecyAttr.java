/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import java.util.Arrays;
import com.sun.source.tree.Tree.Kind;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symbol.MethodSymbol;
import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.Pair;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import com.sun.tools.javac.code.Kinds;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import jp.ac.ritsumei.is.hpcss.ifa4j.checker.ConstraintSet.Constraint;

/**
 *
 * @author bitter_fox
 */
public class SecrecyAttr extends TreeScanner {

    protected static final Context.Key<SecrecyAttr> attrKey =
        new Context.Key<>();

    final Log log;
    final SecrecyTypes secrecyTypes;

    public static SecrecyAttr instance(Context context) {
        SecrecyAttr instance = context.get(attrKey);
        if (instance == null)
            instance = new SecrecyAttr(context);
        return instance;
    }

    protected SecrecyAttr(Context context) {
        context.put(attrKey, this);

        log = Log.instance(context);
        secrecyTypes = SecrecyTypes.instance(context);
    }

    /** Visitor argument: the current environment.
     */
    Env<AttrContext> env;

    public void attrib(Env<AttrContext> env) {
        this.env = env;
        env.tree.accept(this);
    }

    @Override
    public void visitMethodDef(JCMethodDecl that) {
        System.out.println(that);
        Set<SecrecyLattice> lattices = scanLattices(that);
        System.out.println(lattices);

        for (SecrecyLattice lattice : lattices) {
            SecrecyErrorCheckByMethod checker = new SecrecyErrorCheckByMethod(lattice);
            checker.startCheck(that);
        }
    }

    /**
     * This class checks information leaks in a method with a secrecy lattice.
     * Syntax and type system is based on Kurokawa06.
     */
    private class SecrecyErrorCheckByMethod extends JCTree.Visitor {
        private SecrecyLattice lattice;

        private ConstraintSet constraints;

        private IdentityHashMap<JCTree, SecrecyVariable> secrecies = new IdentityHashMap<>();
        private IdentityHashMap<JCTree, SecrecyVariable> heapEffects = new IdentityHashMap<>();

        private SecrecyErrorCheckByMethod(SecrecyLattice lattice) {
            this.lattice = lattice;
            constraints = new ConstraintSet(lattice);
        }

        private void startCheck(JCMethodDecl methodDecl) {
            methodDecl.accept(this);
        }

        @Override
        public void visitTree(JCTree that) {
            System.out.println("Unknown element: " + that);
            if (that instanceof JCTree.JCExpression) {
                secrecies.put(that, lattice.anyVar(that.pos));
            } else if (that instanceof JCTree.JCStatement) {
                secrecies.put(that, lattice.anyVar(that.pos));
                heapEffects.put(that, lattice.anyVar(that.pos));
            }
        }

        private SecrecyVariable resSecrecy;
        /**
         * C-MDEC
         * @param that
         */
        @Override
        public void visitMethodDef(JCTree.JCMethodDecl that) {
            resSecrecy = secrecy(that.restype.type, that.restype.pos);
            if (that.body != null) {
                that.body.accept(this);
            }
            SecrecyVariable he = heapEffect(that.sym, that.pos);
            newConstraint(he, heapEffects.get(that.body), that.pos);

            if (!that.restype.type.hasTag(TypeTag.VOID) && resSecrecy.type.isAny()) {
                log.error(that.restype.pos, "res.secrecy.not.found", that.sym.location());
            }
            if (he.type.isAny()) {
                log.error(that.pos, "he.secrecy.not.found", that.sym.location());
            }

            System.out.println(constraints);
            Map<String, SecrecyType> solve = constraints.solve();
            System.out.println(solve);
            boolean success = !solve.isEmpty();
            if (!success) {
                log.error(that.pos,
                        "invalid.information.flow",
                        that.sym.location());
            }
        }

        @Override
        public void visitReturn(JCTree.JCReturn that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            SecrecyVariable heapEffect = lattice.anyVar(that.pos);
            heapEffects.put(that, heapEffect);

            newConstraint(secVar, resSecrecy, that.pos);

            that.expr.accept(this);
            newConstraint(secrecies.get(that.expr), resSecrecy, that.pos);

//            that.expr.accept(this);
//            that.setSecrecy(resSecrecy);
//            that.setHeapEffect(lattice.anyVar(that.pos));
//            newConstraint(resSecrecy, that.getSecrecy(lattice), that.pos);
        }

        /**
         * C-BLOCK
         * @param that
         */
        @Override
        public void visitBlock(JCTree.JCBlock that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            SecrecyVariable heapEffect = lattice.anyVar(that.pos);
            heapEffects.put(that, heapEffect);

            that.stats.forEach(t -> {
                t.accept(this);
                newConstraint(secVar, secrecies.get(t), that.pos);
                newConstraint(heapEffect, heapEffects.get(t), that.pos);
                addExceptions(that, getExceptions(t));
            });
            getExceptions(that).stream()
                    .map(p -> p.snd)
                    .forEach(k -> that.stats.forEach(t -> {
                        newConstraint(k, secrecies.get(t), that.pos);
                        newConstraint(k, heapEffects.get(t), that.pos);
                    }));
        }

        private void newConstraint(SecrecyVariable low, SecrecyVariable high, int pos) {
            constraints.add(new Constraint(low, high, pos));
        }

        /**
         * C-IF
         * @param that
         */
        @Override
        public void visitIf(JCTree.JCIf that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            SecrecyVariable heapEffect = lattice.anyVar(that.pos);
            heapEffects.put(that, heapEffect);

            that.cond.accept(this);
            newConstraint(secrecies.get(that.cond), secVar, that.pos);
            newConstraint(secrecies.get(that.cond), heapEffect, that.pos);

            that.thenpart.accept(this);
            newConstraint(secVar, secrecies.get(that.thenpart), that.pos);
            newConstraint(heapEffect, heapEffects.get(that.thenpart), that.pos);
            addExceptions(that, getExceptions(that.thenpart));

            if (that.elsepart != null) {
                that.elsepart.accept(this);
                newConstraint(secVar, secrecies.get(that.elsepart), that.pos);
                newConstraint(heapEffect, heapEffects.get(that.elsepart), that.pos);
                addExceptions(that, getExceptions(that.elsepart));
            }
        }

        @Override
        public void visitParens(JCTree.JCParens that) {
            that.expr.accept(this);
            secrecies.put(that, secrecies.get(that.expr));
        }

        @Override
        public void visitAssign(JCAssign that) {
            System.out.println("[WARNING] Assign in Expression is not checked");
        }

        @Override
        public void visitNewClass(JCNewClass that) {
            System.out.println("[WARNING] NewClass in Expression is not checked");
        }

        @Override
        public void visitExec(JCExpressionStatement that) {
            JCExpression expr = that.expr;
            switch (expr.getKind()) {
                case ASSIGNMENT: {
                    visitAssignInStatement((JCAssign)expr, that);
                    break;
                }
                default:
                    System.out.printf("[WARNING] %s in ExpressionStatement is not checked",  expr.getKind());
                    break;
            }
        }
        // where
        /**
         * C-ASSIGN1, C-ASSIGN2, C-NEW, C-CALL
         * @param that
         * @param parent
         */
        public void visitAssignInStatement(JCAssign that, JCExpressionStatement parent) {
            JCExpression var = that.lhs;
            JCExpression value = that.rhs;

            switch (var.getKind()) {
                case IDENTIFIER: { // ASSIGN1 | NEW | CALL
                    SecrecyVariable secVar = lattice.anyVar(that.pos);
                    secrecies.put(that, secVar);
                    secrecies.put(parent, secVar);

                    SecrecyVariable heapEffect = lattice.anyVar(that.pos);
                    heapEffects.put(parent, heapEffect);

                    var.accept(this);
                    // ηs≦ηx
                    secrecies.put(that, secVar);
                    newConstraint(secVar, secrecies.get(var), that.pos);

                    switch (value.getKind()) {
                        case NEW_CLASS: { // NEW: x = new C
                            JCNewClass newClass = (JCNewClass)value;
                            SecrecyVariable level = level(newClass.type.tsym, newClass.clazz.pos);
                            secrecies.put(newClass, level);
                            // level(C)≦ηx
                            newConstraint(level, secrecies.get(var), that.pos);
                            newConstraint(heapEffect, level, that.pos);
                            break;
                        }
                        case METHOD_INVOCATION: { //CALL: x = e.m({e})
                            JCMethodInvocation methodInvocation = (JCMethodInvocation)value;
                            if (methodInvocation.meth.getKind() == Kind.MEMBER_SELECT) {
                                JCFieldAccess meth = (JCFieldAccess)methodInvocation.meth;

                                meth.selected.accept(this);
                                newConstraint(secrecies.get(meth.selected), secrecies.get(var), that.pos);

                                MethodSymbol methSymbol = (MethodSymbol)meth.sym;
                                Type.MethodType methType = (Type.MethodType)methSymbol.type;

                                // args
                                Iterator<JCExpression> args = methodInvocation.args.iterator();
                                Iterator<VarSymbol> params;
                                if (methSymbol.params == null) {
                                    System.out.println(parent);
                                    params = Arrays.<VarSymbol>asList().iterator();
                                } else {
                                    params = methSymbol.params.iterator();
                                }
                                while (args.hasNext()) {
                                    JCExpression arg = args.next();
                                    arg.accept(this);
                                    if (params.hasNext()) {
                                        VarSymbol param = params.next();
                                        newConstraint(secrecies.get(arg), secrecy(param.type, param.pos), that.pos);
                                    }
                                }

                                // return
                                SecrecyVariable res = secrecy(methType.restype, that.pos);
                                newConstraint(res, secrecies.get(var), that.pos);

                                // heapeffect
                                SecrecyVariable he = heapEffect(methSymbol, that.pos);
                                newConstraint(secrecies.get(meth.selected), he, that.pos);
                                newConstraint(heapEffects.get(parent), he, that.pos);

                                methType.thrown.forEach(t -> {
                                    SecrecyVariable k = lattice.anyVar(that.pos);
                                    SecrecyVariable level = level(t.tsym, that.pos);
                                    newConstraint(secrecies.get(var), k, that.pos);
                                    newConstraint(k, level, that.pos);
                                    newConstraint(level, k, that.pos);
                                    addException(parent, Pair.of(t, k));
                                });
                            } else {
                                System.out.println("[WARNING]: You must specify a parent of the method!" + methodInvocation);
                            }
                            break;
                        }
                        default: { // ASSIGN1: x = e
                            value.accept(this);
                            // ηe≦ηx
                            newConstraint(secrecies.get(value), secrecies.get(var), that.pos);
                            break;
                        }
                    }
                    break;
                }
                case MEMBER_SELECT: { // ASSIGN2 e.f = e
                    SecrecyVariable secVar = lattice.anyVar(that.pos);
                    secrecies.put(that, secVar);
                    secrecies.put(parent, secVar);

                    SecrecyVariable heapEffect = lattice.anyVar(that.pos);
                    heapEffects.put(parent, heapEffect);

                    JCFieldAccess fa = (JCFieldAccess)var;

                    fa.selected.accept(this);
                    SecrecyVariable k1 = secrecies.get(fa.selected);

                    value.accept(this);
                    SecrecyVariable k2 = secrecies.get(value);

                    SecrecyVariable x = secrecy(fa.sym.type, ((VarSymbol)fa.sym).pos);
                    newConstraint(k1, x, that.pos);
                    newConstraint(k2, x, that.pos);
                    newConstraint(heapEffect, x, that.pos);
                    break;
                }
                default: {
                    // あり得る？
                    super.visitAssign(that);
                    break;
                }
            }
        }

        @Override
        public void visitThrow(JCTree.JCThrow that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            SecrecyVariable he = lattice.anyVar(that.pos);
            heapEffects.put(that, he);

            SecrecyVariable e = lattice.anyVar(that.pos);
            addException(that, Pair.of(that.expr.type, e));

            if (that.expr.getKind() == Kind.NEW_CLASS) {
                SecrecyVariable level = level(that.expr.type.tsym, that.expr.pos);
                newConstraint(secVar, e, that.pos);
                newConstraint(he, e, that.pos);
                newConstraint(e, level, that.pos);
                newConstraint(level, e, that.pos);
            } else {
                System.out.println("[WARNING] throw statement must be appeared with NEW_CLASS: " + that);
            }
        }

        @Override
        public void visitTry(JCTree.JCTry that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            SecrecyVariable he = lattice.anyVar(that.pos);
            heapEffects.put(that, he);

            that.body.accept(this);
            newConstraint(secVar, secrecies.get(that.body), that.pos);
            newConstraint(he, heapEffects.get(that.body), that.pos);

            // ε += {(E, k) | (E, k) ∈ ε' ∧ E ∉ {E1, En}}
            List<Type> catchedExceptions = that.catchers.stream()
                    .map(c -> c.param.type)
                    .collect(Lists.collector());

            Set<Pair<Type, SecrecyVariable>> exceptions = new HashSet<>();
            for (Pair<Type, SecrecyVariable> p : getExceptions(that.body)) {
                if (!catchedExceptions.contains(p.fst)) {
                    exceptions.add(p);
                }
            }
            addExceptions(that, exceptions);
//            that.addExceptions(that.body.getExceptions(lattice).stream()
//                    .filter(p -> catchedExceptions.stream().noneMatch(t -> p.fst.equals(t)))
//                    .collect(Collectors.toSet()));

            that.catchers.forEach(c -> {
                c.body.accept(this);
                addExceptions(that, getExceptions(c.body)); // ε += εi
                newConstraint(secVar, secrecies.get(c.body), that.pos);
                newConstraint(he, heapEffects.get(c.body), that.pos);

                SecrecyVariable level = level(c.param.type.tsym, that.pos);
                newConstraint(level, secrecies.get(c.body), that.pos);
                newConstraint(level, secrecies.get(c.body), that.pos);
            });
        }

        private IdentityHashMap<JCStatement, Set<Pair<Type, SecrecyVariable>>> exceptions = new IdentityHashMap<>();

        Set<Pair<Type, SecrecyVariable>> getExceptions(JCStatement that) {
            return exceptions.computeIfAbsent(that, t -> new HashSet<>());
        }
        void addException(JCStatement that, Pair<Type, SecrecyVariable> ex) {
            exceptions.computeIfAbsent(that, t -> new HashSet<>())
                    .add(ex);
        }
        void addExceptions(JCStatement that, Set<Pair<Type, SecrecyVariable>> exs) {
            exceptions.computeIfAbsent(that, t -> new HashSet<>())
                    .addAll(exs);
        }

        /**
         * C-VAR C-THIS
         * @param that
         */
        @Override
        public void visitIdent(JCIdent that) {
            // TODO ClassSymbol
            SecrecyVariable eta = that.sym.kind == Kinds.VAR ? secrecyForVariable((VarSymbol)that.sym) : lattice.anyVar(that.pos);
            SecrecyVariable kappa = lattice.anyVar(that.pos);
            secrecies.put(that, kappa);
            newConstraint(eta, kappa, that.pos);
            newConstraint(kappa, eta, that.pos);
        }

        /**
         * C-CONST
         * @param that
         */
        @Override
        public void visitLiteral(JCLiteral that) {
            SecrecyVariable k = lattice.anyVar(that.pos);
            secrecies.put(that, k);
        }

        /**
         * C-COMP
         * @param that
         */
        @Override
        public void visitBinary(JCBinary that) {
            SecrecyVariable k = lattice.anyVar(that.pos);
            secrecies.put(that, k);

            that.lhs.accept(this);
            newConstraint(secrecies.get(that.lhs), k, that.pos);

            that.rhs.accept(this);
            newConstraint(secrecies.get(that.rhs), k, that.pos);
        }

        /**
         * C-FIELD
         * @param that
         */
        @Override
        public void visitSelect(JCFieldAccess that) {
            SecrecyVariable secVar = lattice.anyVar(that.pos);
            secrecies.put(that, secVar);

            that.selected.accept(this);
            SecrecyVariable s = secrecies.get(that.selected);
            newConstraint(s, secVar, that.pos);

            SecrecyVariable x = secrecy(that.sym.type, ((VarSymbol)that.sym).pos);
            newConstraint(x, secVar, that.pos);
        }

        /**
         * C-CAST
         * @param that
         */
        @Override
        public void visitTypeCast(JCTree.JCTypeCast that) {
            that.expr.accept(this);
            secrecies.put(that, secrecies.get(that.expr));
        }

        /**
         * C-IS
         * @param that
         */
        @Override
        public void visitTypeTest(JCTree.JCInstanceOf that) {
            that.expr.accept(this);
            secrecies.put(that, secrecies.get(that.expr));
        }

        private final IdentityHashMap<VarSymbol, SecrecyVariable> secreciesForVariable = new IdentityHashMap<>();
        private SecrecyVariable secrecyForVariable(VarSymbol symbol) {
            return secreciesForVariable.computeIfAbsent(symbol, s -> secrecy(s.type, s.pos));
        }

        private SecrecyVariable secrecy(Type type, int pos) {
            List<SecrecyType> typeList = secrecyTypes.secrecyToSecrecyTypes(type.getAnnotationMirrors());
            return secrecyVariableFromTypes(typeList, pos);
        }

        private SecrecyVariable level(Symbol.TypeSymbol clazz, int pos) {
            List<SecrecyType> typeList = secrecyTypes.secrecyToSecrecyTypes(clazz.getAnnotationMirrors());
            return secrecyVariableFromTypes(typeList, pos);
        }

        private SecrecyVariable heapEffect(Symbol.MethodSymbol meth, int pos) {
            List<SecrecyType> typeList = secrecyTypes.heapEffectToSecrecyTypes(meth.getAnnotationMirrors());
            return secrecyVariableFromTypes(typeList, pos);
        }
        // where
        private SecrecyVariable secrecyVariableFromTypes(List<SecrecyType> typeList, int pos) {
            Optional<SecrecyType> type = Optional.empty();

            for (SecrecyType t : typeList) {
                if (t.lattice.equals(lattice)) {
                    type = Optional.of(t);
                    break;
                }
            }
            return type.map(t -> t.instanceVariable(pos))
                    .orElseGet(() -> lattice.anyVar(pos));

//            return typeList.stream().filter(t -> t.lattice.equals(lattice))
//                    .findFirst()
//                    .map(t -> t.instanceVariable(pos))
//                    .orElseGet(() -> lattice.anyVar(pos));
        }
    }

    private Set<SecrecyLattice> scanLattices(JCTree tree) {
        SecrecyLatticeScanner sls = new SecrecyLatticeScanner();
        sls.scan(tree);
        return sls.lattices;
    }
    // where
    private class SecrecyLatticeScanner extends TreeScanner {

        private Set<SecrecyLattice> lattices;

        private SecrecyLatticeScanner() {
            this.lattices = new HashSet<>();
        }

        @Override
        public void visitIdent(JCTree.JCIdent that) {
            if (that.sym != null) {
                addAllBySecrecyType(
                        secrecyTypes.secrecyToSecrecyTypes(that.sym.type.getAnnotationMirrors()));
            }
        }

        @Override
        public void visitNewClass(JCTree.JCNewClass that) {
            super.visitNewClass(that);
            addAllBySecrecyType(
                    secrecyTypes.secrecyToSecrecyTypes(that.type.tsym.getAnnotationMirrors()));
            // HeapEffect?
        }

        @Override
        public void visitApply(JCMethodInvocation tree) {
            super.visitApply(tree);
            Type.MethodType mtype = (Type.MethodType)tree.meth.type;

            if (mtype == null) { return; }


            // args
            mtype.argtypes.forEach(pt -> addAllBySecrecyType(
                    secrecyTypes.secrecyToSecrecyTypes(pt.getAnnotationMirrors())));
            // return
            addAllBySecrecyType(
                    secrecyTypes.secrecyToSecrecyTypes(mtype.restype.getAnnotationMirrors()));

            // heap effect
            addAllBySecrecyType(
                    secrecyTypes.heapEffectToSecrecyTypes(TreeInfo.symbol(tree.meth).getAnnotationMirrors()));
            // TODO: exceptions
        }

        // TODO method declare?
        // TODO this, super?

        private void addAllBySecrecyType(List<SecrecyType> secrecies) {
            for (SecrecyType sec : secrecies) {
                lattices.add(sec.lattice);
            }
        }
    }
}
