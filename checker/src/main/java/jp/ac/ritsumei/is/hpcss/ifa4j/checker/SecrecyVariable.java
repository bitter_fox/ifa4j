/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author bitter_fox
 */
public class SecrecyVariable {
    private static AtomicLong ID_SOURCE = new AtomicLong(0);

    public SecrecyType type;
    public int pos;
    public long id;

    public SecrecyVariable(SecrecyType type, int pos) {
        this.type = type;
        this.pos = pos;
        this.id = ID_SOURCE.getAndIncrement();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.type);
        hash = 41 * hash + this.pos;
        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SecrecyVariable other = (SecrecyVariable) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (this.pos != other.pos) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SecrecyVariable{" + "type=" + type + ", pos=" + pos + ", id=" + id + '}';
    }

}
