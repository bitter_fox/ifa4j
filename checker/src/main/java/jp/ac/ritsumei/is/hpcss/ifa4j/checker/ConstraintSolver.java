/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import com.sun.tools.javac.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiPredicate;
import java.util.function.Function;
import jp.ac.kobe_u.cs.cream.Constraint;
import jp.ac.kobe_u.cs.cream.DefaultSolver;
import jp.ac.kobe_u.cs.cream.Domain;
import jp.ac.kobe_u.cs.cream.Network;
import jp.ac.kobe_u.cs.cream.Solution;
import jp.ac.kobe_u.cs.cream.Solver;
import jp.ac.kobe_u.cs.cream.Trail;
import jp.ac.kobe_u.cs.cream.Variable;

/**
 *
 * @author bitter_fox
 */
class ConstraintSolver {
    private Network network;
    private SecrecyDomain domain;

    public ConstraintSolver(SecrecyLattice lattice) {
        network = new Network();
        domain = new SecrecyDomain(lattice);
    }

    void addConstraints(ConstraintSet constraints) {
        Map<SecrecyVariable, Variable> vars = new IdentityHashMap<>();

        constraints.stream()
                .forEach(c ->
                    new SecrecyOrder(network,
                            asVariable(c.low, vars),
                            asVariable(c.high, vars)));
        System.out.println(vars.size());
        System.out.println(vars);
    }

    Variable asVariable(SecrecyVariable var, Map<SecrecyVariable, Variable> vars) {
        boolean isAny = var.type.isAny();
        return vars.computeIfAbsent(var,
                v -> new Variable(network,
                        isAny ? domain.clone() : domain.singleton(var.type.name().toString()),
                        Integer.toString(v.hashCode())));
    }

    Map<String, SecrecyType> solve() {
        if (System.getProperty("tiifa.solver.verbose") != null) {
            System.out.println("---");
            System.out.print(network);
            System.out.println("---");
        }

        Solver solver = new DefaultSolver(network);
        Solution solution = solver.findFirst();

        if (solution == null) {
            return Collections.emptyMap();
        }

        //System.out.println("===");
        //System.out.println(solution);
        Map<String, SecrecyType> result = new TreeMap<>();
        for (Variable var : network.getVariables()) {
            SecrecyDomain d = (SecrecyDomain)solution.getDomain(var);
            result.put(var.getName(), d.secrecyTypes().iterator().next());
        }
        //System.out.println(result);

        return Collections.unmodifiableMap(result);
    }

    /**
     * 制約を解きすべての解を求める
     * @return 解のコレクション。
     *         解が見つからなかった場合は大きさが 0 のコレクション。
     */
    public Collection<Map<String, SecrecyType>> solveAll() {
        if (System.getProperty("tiifa.solver.verbose") != null) {
            System.out.println("---");
            System.out.print(network);
            System.out.println("---");
        }

        Solver solver = new DefaultSolver(network);
        Collection<Map<String, SecrecyType>> solutions = new ArrayList<>();

        for (solver.start(); solver.waitNext(); solver.resume()) {
            if (System.getProperty("tiifa.solver.verbose") != null) {
                System.out.println("===");
                System.out.println(solver.getSolution());
            }
            Solution sol = solver.getSolution();
            Map<String, SecrecyType> map = new TreeMap<>();
            for (Variable var : network.getVariables()) {
                SecrecyDomain d = (SecrecyDomain)sol.getDomain(var);
                map.put(var.getName(), d.secrecyTypes().iterator().next());
            }
            solutions.add(map);
        }
        solver.stop();

        return Collections.unmodifiableCollection(solutions);
    }

    private final class SecrecyDomain extends Domain {
        private SecrecyLattice lattice;
        private Set<String> labels;

        private SecrecyDomain() {
            labels = new HashSet<>();
        }

        SecrecyDomain(SecrecyLattice lattice) {
            this.lattice = lattice;
            labels = new HashSet<>(lattice.namesAsString());
        }

        SecrecyDomain singleton(String label) {
            SecrecyDomain sd = new SecrecyDomain();
            sd.lattice = lattice;
            sd.labels.add(label);
            return sd;
        }

        List<SecrecyType> secrecyTypes() {
            return labels.stream()
                    .map(lattice::fromName)
                    .collect(Lists.collector());
        }

        @Override
        public boolean isEmpty() {
            return labels.isEmpty();
        }

        @Override
        public int size() {
            return labels.size();
        }

        @Override
        public SecrecyDomain clone() {
            SecrecyDomain sd = new SecrecyDomain();
            sd.lattice = lattice;
            sd.labels.addAll(labels);
            return sd;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 71 * hash + Objects.hashCode(this.lattice);
            hash = 71 * hash + Objects.hashCode(this.labels);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final SecrecyDomain other = (SecrecyDomain) obj;
            if (!Objects.equals(this.lattice, other.lattice)) {
                return false;
            }
            if (!Objects.equals(this.labels, other.labels)) {
                return false;
            }
            return true;
        }

        @Override
        public boolean equals(Domain domain) {
            return this.equals((Object)domain);
        }

        @Override
        public Iterator<Domain> elements() {
            return labels.stream()
                    .<Domain>map(l -> {
                        SecrecyDomain d = new SecrecyDomain();
                        d.lattice = lattice;
                        d.labels.add(l);
                        return d;
                    })
                    .iterator();
        }

        @Override
        public Object element() throws NoSuchElementException {
            System.out.println("element");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean contains(Object o) {
            System.out.println("contains");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Domain insert(Object o) {
            System.out.println("insert");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Domain delete(Object o) {
            System.out.println("delete");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Domain cap(Domain domain) {
            System.out.println("cap");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Domain cup(Domain domain) {
            System.out.println("cup");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Domain difference(Domain domain) {
            System.out.println("difference");
            System.exit(0);
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public SecrecyDomain retainLowerThan(SecrecyDomain criteria) {
            return retainThan(criteria, SecrecyLattice::bottom, SecrecyType::lowerThanOrEquals);
        }

        public SecrecyDomain retainUpperThan(SecrecyDomain criteria) {
            return retainThan(criteria, SecrecyLattice::top, (t1, t2) -> t2.lowerThanOrEquals(t1));
        }

        private SecrecyDomain retainThan(
                SecrecyDomain criteria,
                Function<SecrecyLattice, SecrecyType> initBound,
                BiPredicate<SecrecyType, SecrecyType> comparator) {
            if (!lattice.equals(criteria.lattice)) {
                return this;
            }

            SecrecyType bound = criteria.labels.stream()
                    .map(criteria.lattice::fromName)
                    .reduce(initBound.apply(criteria.lattice), (t1, t2) -> comparator.test(t1, t2) ? t2 : t1);

            if (System.getProperty("tiifa.solver.verbose") != null) {
                System.out.print(labels + (initBound.apply(criteria.lattice) == criteria.lattice.bottom() ? "<:" : ":>") + criteria.labels + "(" + bound.name() + ")");
            }

            SecrecyDomain d = new SecrecyDomain();
            d.lattice = lattice;

            d.labels = new HashSet<>();
            for (String l : labels) {
                if (comparator.test(lattice.fromName(l), bound)) {
                    d.labels.add(l);
                }
            }
//            d.labels = labels.stream()
//                    .filter(l -> comparator.test(lattice.fromName(l), bound))
//                    .collect(Collectors.toCollection(HashSet::new));

            if (System.getProperty("tiifa.solver.verbose") != null) {
                System.out.println(" -> " + d.labels);
            }

            return d;
        }

        @Override
        public String toString() {
            return "SecrecyDomain{" + "lattice=" + lattice + ", labels=" + labels + '}';
        }
    }

    private class SecrecyOrder extends Constraint {
        private Network network;
        private Variable low;
        private Variable high;

        public SecrecyOrder(Network network, Variable low, Variable high) {
            super(network);
            this.network = network;
            this.low = low;
            this.high = high;
        }

        @Override
        public Constraint copy(Network ntwrk) {
            return new SecrecyOrder(network, Constraint.copy(low, network), Constraint.copy(high, network));
        }

        @Override
        public boolean isModified() {
            return low.isModified() || high.isModified();
        }

        @Override
        public boolean satisfy(Trail trail) {
            SecrecyDomain lowDomain = (SecrecyDomain)low.getDomain();
            SecrecyDomain highDomain = (SecrecyDomain)high.getDomain();

            // lowDomain に high 以下のみ残す
            if (System.getProperty("tiifa.solver.verbose") != null) {
                System.out.println(low + "<:" + high + "  ");
            }
            lowDomain = lowDomain.retainLowerThan(highDomain);
            if (lowDomain.isEmpty()) {
                return false;
            }
            low.updateDomain(lowDomain, trail);

            // highDomain に low 以上のみ残す
            if (System.getProperty("tiifa.solver.verbose") != null) {
                System.out.println(high + ":>" + low + "  ");
            }
            highDomain = highDomain.retainUpperThan(lowDomain);
            if (highDomain.isEmpty()) {
                return false;
            }
            high.updateDomain(highDomain, trail);

            /*
            System.out.println("  " + low + ": " + low.getDomain());
            System.out.println("  " + low + ": modified : " + low.isModified());
            System.out.println("  " + high + ": " + high.getDomain());
            System.out.println("  " + high + ": modified : " + high.isModified());
            */

            return true;
        }

        @Override
        public String toString() {
            return low + " < " + high;
        }
    }
}
