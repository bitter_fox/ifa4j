/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import com.sun.tools.javac.code.Attribute;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.sun.tools.javac.util.ListBuffer;

/**
 *
 * @author bitter_fox
 */
public class SecrecyLattice {
    private final Context context;
    private final SecrecyTypes types;
    private final Names names;

    private Type type;

    private List<SecrecyType> secrecies;
    private Map<SecrecyType, List<SecrecyType>> lowerBounds;
    private Map<SecrecyType, List<SecrecyType>> upperBounds;
    private SecrecyType bottom;
    private SecrecyType top;
    final SecrecyType any;

    SecrecyLattice(Context context, Type latticeType) {
        this.context = context;
        types = SecrecyTypes.instance(context);
        names = Names.instance(context);

        this.type = latticeType;
        buildLattice();

        any = new SecrecyType(this, Symtab.instance(context).botType.tsym);
    }

    public SecrecyVariable anyVar(int pos) {
        return any.instanceVariable(pos);
    }

    public SecrecyType fromName(String name) {
        for (SecrecyType s : secrecies) {
            if (s.tsym.name.contentEquals(name)) {
                return s;
            }
        }
        throw new NoSuchElementException();
//        return secrecies.stream()
//                .filter(s -> s.tsym.name.contentEquals(name))
//                .findFirst().orElseThrow(NoSuchElementException::new);
    }
    public SecrecyType fromName(Name name) {
        for (SecrecyType s : secrecies) {
            if (s.tsym.name.equals(name)) {
                return s;
            }
        }
        throw new NoSuchElementException();
//        return secrecies.stream()
//                .filter(s -> s.tsym.name.equals(name))
//                .findFirst().orElseThrow(NoSuchElementException::new);
    }

    public Set<String> namesAsString() {
        return secrecies.stream()
                .map(s -> s.tsym.name)
                .map(Object::toString)
                .collect(Collectors.toSet());
    }

    public SecrecyType bottom() {
        return bottom;
    }

    public SecrecyType top() {
        return top;
    }

    public List<SecrecyType> lowerBoundsOf(SecrecyType type) {
        return lowerBounds.get(type);
    }

    public List<SecrecyType> upperBoundsOf(SecrecyType type) {
        return upperBounds.get(type);
    }

    private void buildLattice() {
//        System.out.println("buildLattice");

        ListBuffer<SecrecyType> secrecies = new ListBuffer<>();
        for (Symbol s : type.tsym.getEnclosedElements()) {
            if (s.isEnum()) {
                secrecies.add(new SecrecyType(this, s));
            }
        }
        this.secrecies = secrecies.toList();
//        secrecies = List.from(() ->
//                type.tsym.getEnclosedElements().stream()
//                        .filter(Symbol::isEnum)
//                        .map(s -> new SecrecyType(this, s))
//                        .iterator());

        lowerBounds = secrecies.stream()
                .collect(Collectors.toMap(Function.identity(), v -> List.of(v)));
        upperBounds = secrecies.stream()
                .collect(Collectors.toMap(Function.identity(), v -> List.of(v)));

        for (SecrecyType sec : secrecies) {
            Attribute.Compound lowerOf = types.findLowerOf(sec.tsym.getAnnotationMirrors());
//            System.out.println(lowerOf);
            if (lowerOf == null) {
                continue;
            }

            Attribute.Array a = (Attribute.Array)types.findMember(lowerOf, names.value);
            List<SecrecyType> upperBounds = Arrays.stream(a.values)
                    .map(v -> (Attribute.Enum)v)
                    .map(v -> fromName(v.value.name))
//                    .peek(System.out::println)
                    .collect(Lists.collector());
            this.upperBounds.computeIfPresent(sec, (s, ss) -> ss.appendList(upperBounds));

            upperBounds.forEach(
                    ub -> this.lowerBounds.computeIfPresent(ub,
                            (s, ss) -> ss.append(sec)));

//            System.out.println(a);
        }

        top = findNoBounds(upperBounds);
        bottom = findNoBounds(lowerBounds);
    }

    private static SecrecyType findNoBounds(Map<SecrecyType, List<SecrecyType>> map) {
        for (Map.Entry<SecrecyType, List<SecrecyType>> e : map.entrySet()) {
            if (e.getValue().size() == 1) {
                return e.getKey();
            }
        }
        throw new NoSuchElementException();
//        return map.entrySet().stream()
//                .filter(e -> e.getValue().size() == 1)
//                .map(Map.Entry::getKey)
//                .findFirst().get();
    }

    @Override
    public String toString() {
        return "SecrecyLattice{" + "type=" + type + '}';
    }
}
