/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import com.sun.tools.javac.code.Attribute;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;
import java.util.HashMap;
import java.util.Map;
import com.sun.tools.javac.jvm.ClassReader;
import com.sun.tools.javac.util.ListBuffer;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.HeapEffectAnnotation;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyAnnotation;

/**
 *
 * @author bitter_fox
 */
class SecrecyTypes {

    protected static final Context.Key<SecrecyTypes> attrKey
            = new Context.Key<SecrecyTypes>();

    private final Context context;
    private final Names names;
    private final ClassReader reader;

    private Map<Symbol, SecrecyLattice> lattices = new HashMap<>();

    public final Type secrecyAnnotationType; // @SecrecyAnnotaion
    public final Type secrecyAnnotationClassType; // @SecrecyAnnotationClass
    public final Type lowerOfAnnotationType; // @LowerOfAnnotation

    public static SecrecyTypes instance(Context context) {
        SecrecyTypes instance = context.get(attrKey);
        if (instance == null) {
            instance = new SecrecyTypes(context);
        }
        return instance;
    }

    private SecrecyTypes(Context context) {
        this.context = context;
        names = Names.instance(context);
        reader = ClassReader.instance(context);

        secrecyAnnotationType = enterClass("SecrecyAnnotation");
        secrecyAnnotationClassType = enterClass("SecrecyAnnotationClass");
        lowerOfAnnotationType = enterClass("LowerOfAnnotation");
    }

    private Type enterClass(String s) {
        return reader.enterClass(names.fromString(s)).type;
    }

    private boolean isSecrecy(Attribute.Compound am) {
        return am.type.tsym.getAnnotation(SecrecyAnnotation.class) != null; // SHOULD USE SYMTAB?
    }

    private boolean isHeapEffect(Attribute.Compound am) {
        return am.type.tsym.getAnnotation(HeapEffectAnnotation.class) != null;
    }

    private List<? extends Attribute.Compound> onlySecrecies(List<? extends Attribute.Compound> ams) {
        ListBuffer<Attribute.Compound> list = new ListBuffer<>();
        for (Attribute.Compound am : ams) {
            if (this.isSecrecy(am)) {
                list.add(am);
            }
        }
        return list.toList();

//        return List.from(() -> ams.stream()
//                .filter(this::isSecrecy)
//                .iterator());
    }

    private List<? extends Attribute.Compound> onlyHeapEffects(List<? extends Attribute.Compound> ams) {
        ListBuffer<Attribute.Compound> list = new ListBuffer<>();
        for (Attribute.Compound am : ams) {
            if (this.isHeapEffect(am)) {
                list.add(am);
            }
        }
        return list.toList();

//        return List.from(() -> ams.stream()
//                .filter(this::isHeapEffect)
//                .iterator());
    }

    List<SecrecyType> secrecyToSecrecyTypes(List<? extends Attribute.Compound> secrecies) {
        return onlySecrecies(secrecies).stream()
                .map(this::toSecrecyType)
                .collect(Lists.collector());
    }

    List<SecrecyType> heapEffectToSecrecyTypes(List<? extends Attribute.Compound> secrecies) {
        return onlyHeapEffects(secrecies).stream()
                .map(this::toSecrecyType)
                .collect(Lists.collector());
    }

    SecrecyType toSecrecyType(Attribute.Compound secrecy) {
        if (!(isSecrecy(secrecy) || isHeapEffect(secrecy))) {
            return null;
        }

        SecrecyLattice lat = findSecrecyLatticeFromSecrecy(secrecy);

        Name name = ((Attribute.Enum) findMember(secrecy, names.value)).value.name;
        return lat.fromName(name);
    }
    // where
//    private Attribute findValueOfAnnotation(Attribute.TypeCompound anno, String name) {
//        return anno.values.stream()
//                .filter(p -> p.fst.name.contentEquals(name))
//                .map(p -> p.snd)
//                .findAny().orElse(null);
//    }

    SecrecyLattice findSecrecyLatticeFromSecrecy(/*@Secrecy(H)*/Attribute.Compound secrecy) {
        List<Attribute.Compound> mirrors = secrecy.type.tsym.owner.getAnnotationMirrors();
        System.out.println(mirrors);
        Attribute.Compound sac = findSecrecyAnnotationsClass(mirrors);
        Attribute.Class clazz = (Attribute.Class) findMember(sac, names.fromString("clazz"));
        Type latticeType = clazz.classType;

        return lattices.computeIfAbsent(latticeType.tsym, tsym -> new SecrecyLattice(context, latticeType));
    }

    // where
    private Attribute.Compound findSecrecyAnnotationsClass(List<Attribute.Compound> annos) {
        return findAnnotationByType(annos, secrecyAnnotationClassType);
    }

    Attribute.Compound findLowerOf(List<Attribute.Compound> annos) {
        return findAnnotation(annos, this::isAnnotatedByLowerOfAnnotation);
    }

    boolean isAnnotatedByLowerOfAnnotation(Attribute.Compound anno) {
        return findLowerOfAnnotation(anno.type.tsym.getAnnotationMirrors()) != null;
    }

    Attribute.Compound findLowerOfAnnotation(List<? extends Attribute.Compound> annos) {
        return findAnnotationByType(annos, lowerOfAnnotationType);
    }

    private interface Predicate<T> {

        boolean test(T t);
    }

    private <T extends Attribute> T findAnnotation(List<T> annos, Predicate<? super T> test) {
        for (T t : annos) {
            if (test.test(t)) {
                return t;
            }
        }
        return null;
//        return annos.stream()
//                .filter(test)
//                .findFirst().orElse(null);
    }

    private Attribute.Compound findAnnotationByType(List<? extends Attribute.Compound> annos, Type target) {
        return findAnnotation(annos, am -> {
//                am.type.equals(target) // Doesn't work well
            System.out.println(am.type.tsym.getQualifiedName());
            System.out.println(target.tsym.getQualifiedName());
            System.out.println(am.type.tsym.getQualifiedName().contentEquals(target.tsym.getQualifiedName()));
            return am.type.tsym.getQualifiedName().contentEquals(target.tsym.getQualifiedName()); // FIXME
        });
    }

    // FIXME
    Attribute findMember(Attribute.Compound anno, Name member) {
        return anno.values.stream()
                .filter(m -> m.fst.name.contentEquals(member))
                .map(m -> m.snd)
                .findAny().get();
    }

}
