/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.checker;

import java.util.stream.Collector;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;

/**
 *
 * @author bitter_fox
 */
public class Lists {

    public static <T> Collector<T, ?, List<T>> collector() {
        return Collector.<T, ListBuffer<T>, List<T>>of(
                ListBuffer::new,
                ListBuffer::append,
                ListBuffer::appendList,
                ListBuffer::toList);
    }

}
