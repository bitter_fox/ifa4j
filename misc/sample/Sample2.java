package sample;

import static sample.MySecrecy.*;
import sample.MySecrecyAnnotations.*;

public class Sample {
    @HeapEffect(L)
    public @Secrecy(H) int method(@Secrecy(H) int n) throws MyException {
        if (n > 0) {
            return n;
        }
        return 0;
    }

    @HeapEffect(L)
    public void throwException() throws MyException {
        method(1);
        throw new Exception();
    }
}

@Secrecy(H)
class HClass {
}

@Secrecy(L)
class MyException extends Exception {
}
