package sample;

import static sample.MySecrecy.*;
import sample.MySecrecyAnnotations.*;

public class Sample {
    public void illegalMethod() {
        @Secrecy(L) boolean cond = true;
        @Secrecy(H) int ok;

        if (cond) {
            try {
                ok = this.method(1);
            } catch (MyException e) {}
        } else {
            ok = 0;
        }

        @Secrecy(H) HClass hclass;
        hclass = new HClass();
    }

    @HeapEffect(L)
    public @Secrecy(H) int method(@Secrecy(H) int n) throws MyException {
        if (n > 0) {
            return n;
        }// else {
            throw new MyException();
//        }
    }

    @HeapEffect(L)
    public void throwException() throws MyException {
        method(1);
        throw new Exception();
    }
}

@Secrecy(H)
class HClass {
}

@Secrecy(L)
class MyException extends Exception {
}

/*
144: BLOCK of illegalMethod()
κ@144 ⊑@144κ@174, // boolean cond
κ@144 ⊑@144κ@211, // int ok
κ@144 ⊑@144κ@224, // if (cond) {} else {}
κ@144 ⊑@144κ@331, // HClass hclass
κ@144 ⊑@144κ@354, // hclass = new HClass()

224: IF
κ@228 ⊑@224κ@224, // cond
κ@224 ⊑@224κ@234, // thenpart
κ@224 ⊑@224κ@271, // elsepart

228: 224.cond
228: cond: (boolean, H)
H@174 ⊑@228κ@228,
κ@228 ⊑@228H@174,

234: BLOCK of 224.thenpart
κ@234 ⊑@234κ@251,

248: ok: (int, L)
L@211 ⊑@248κ@248,
κ@248 ⊑@248L@211,

251: =(ok, 1)
κ@251 ⊑@251κ@248,
κ@253 ⊑@251κ@248,

271: BLOCK of 224.elsepart
κ@271 ⊑@271κ@288,

285: ok: (int, L)
L@211 ⊑@285κ@285,
κ@285 ⊑@285L@211,

288: =(ok, 0)
κ@288 ⊑@288κ@285,
κ@290 ⊑@288κ@285,

347: hclass(HClass, L)
L@331 ⊑@347κ@347,
κ@347 ⊑@347L@331,

354: =(hclass, new HClass)
κ@354 ⊑@354κ@347,
H@360 ⊑@354κ@347}
*/
