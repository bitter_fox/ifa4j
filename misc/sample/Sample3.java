package sample;

import static sample.MySecrecy.*;
import sample.MySecrecyAnnotations.*;
@Secrecy(L)
public class Sample3 {

    @Secrecy(L) String output;
    @Secrecy(L)
    @HeapEffect(L)
    Void print(@Secrecy(L) String s) {
        this.output = s;
        return null;
    }

    @Secrecy(H) String password;
    @Secrecy(L) String a() {
        int len;
        String ret;
        len = password.length();
        if (len < 8) {
            ret = "aa";
        } else {
            ret = "";
        }
        return ret;
    }
}
