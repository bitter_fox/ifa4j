
// package sample;: package
// sample.MySecrecy: enumFullName
// MySecrecyAnnotations: nameOfAnnotationClass

package sample;

import javax.annotation.Generated;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.*;

@Generated("sample.MySecrecy")
@SecrecyAnnotationClass(clazz=sample.MySecrecy.class, className="sample.MySecrecy")
public final class MySecrecyAnnotations {
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.CLASS)
    @LowerOfAnnotation
    public @interface LowerOf {
        sample.MySecrecy[] value();
    }

    @Target({ElementType.TYPE_USE, ElementType.TYPE_PARAMETER, ElementType.TYPE})
    @Retention(RetentionPolicy.CLASS)
    @SecrecyAnnotation
    public @interface Secrecy {
        sample.MySecrecy value();
    }

    @Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
    @Retention(RetentionPolicy.CLASS)
    @HeapEffectAnnotation
    public @interface HeapEffect {
        sample.MySecrecy value();
    }
}
