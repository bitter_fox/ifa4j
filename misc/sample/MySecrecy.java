package sample;

import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.*;
import sample.MySecrecyAnnotations;

@SecrecyLattice
public enum MySecrecy {
    @Top H,
    @sample.MySecrecyAnnotations.LowerOf(H) L;
}
