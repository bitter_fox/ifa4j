package sample;

import static sample.MySecrecy.*;
import sample.MySecrecyAnnotations.*;

@Secrecy(H)
class MyException extends Exception {}

interface LogWriter {void write(String s);}

@Secrecy(H)
class Echo {

    @Secrecy(L) String output;

    @HeapEffect(L)
    void echo() {
        @Secrecy(L)
        String input;
        @Secrecy(L)
        int len;

        @Secrecy(L)
            Void tmp;

        input = this.read();
        len = input.length();
        if (len == 0) {
            try {
                tmp = this.log("zero-length");
            } catch (@Secrecy(H) MyException e) {
                tmp = this.print("i/o error");
            }
        } else {
            tmp = this.print(input);
        }
    }

    @Secrecy(L)
    Void log(String s) throws MyException {
        return null;
    }

    @HeapEffect(L)
    @Secrecy(L) String read() {
        return "";
    }

    @Secrecy(L)
    Void print(@Secrecy(L) String s) {
        this.output = s;
        return null;
    }
}
