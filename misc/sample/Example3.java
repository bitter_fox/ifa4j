/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import static sample.MySecrecy.H;
import static sample.MySecrecy.L;
import sample.MySecrecyAnnotations.HeapEffect;
import sample.MySecrecyAnnotations.Secrecy;

@Secrecy(H) class HighException extends Exception {}

@Secrecy(L) public class Example3 {
    @Secrecy(H) String secretLog;
            Void v;

    @HeapEffect(H)
    @Secrecy(L) Void use() {
        try {
            v = this.method();
        } catch (HighException e) {
            secretLog = "";
        }

        if (lowResult == 1) {
            Object o;
            o = System.out.printf("high is definily false; invalid information flow");
        }
        return null;
    }

    @Secrecy(H) boolean high;
    @Secrecy(L) boolean low;

    @Secrecy(L) int lowResult = 0;

    @HeapEffect(H)
    @Secrecy(L)
    Void method() throws HighException {
        if (low) {
            if (high) {
                throw new HighException();
            } else {

            }
            lowResult = 1;
        }
        return null;
    }
}
