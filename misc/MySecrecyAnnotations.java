

import javax.annotation.Generated;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.*;

@Generated("MySecrecy")
@SecrecyAnnotationClass(clazz=MySecrecy.class, className="MySecrecy")
public final class MySecrecyAnnotations {
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.CLASS)
    @LowerOfAnnotation
    public @interface LowerOf {
        MySecrecy[] value();
    }

    @Target({ElementType.TYPE_USE, ElementType.TYPE_PARAMETER, ElementType.TYPE})
    @Retention(RetentionPolicy.CLASS)
    @SecrecyAnnotation
    public @interface Secrecy {
        MySecrecy value();
    }

    @Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
    @Retention(RetentionPolicy.CLASS)
    @HeapEffectAnnotation
    public @interface HeapEffect {
        MySecrecy value();
    }
}
