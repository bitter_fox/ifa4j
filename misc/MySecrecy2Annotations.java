

import javax.annotation.Generated;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.*;

@Generated("Test.MySecrecy2")
@SecrecyAnnotationClass(clazz=Test.MySecrecy2.class, className="Test.MySecrecy2")
public final class MySecrecy2Annotations {
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.CLASS)
    @LowerOfAnnotation
    public @interface LowerOf {
        Test.MySecrecy2[] value();
    }

    @Target({ElementType.TYPE_USE, ElementType.TYPE_PARAMETER, ElementType.TYPE})
    @Retention(RetentionPolicy.CLASS)
    @SecrecyAnnotation
    public @interface Secrecy {
        Test.MySecrecy2 value();
    }

    @Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
    @Retention(RetentionPolicy.CLASS)
    @HeapEffectAnnotation
    public @interface HeapEffect {
        Test.MySecrecy2 value();
    }
}
