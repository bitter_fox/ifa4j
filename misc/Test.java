
public class Test {
    @SecrecyLattice
    public static enum MySecrecy {
        @Top H,
        @MySecrecyAnnotations.LowerOf(H) L;
    }
    @SecrecyLattice
    public static enum MySecrecy2 {
        @Top H,
        @MySecrecy2Annotations.LowerOf(H) L;
    }

    public static void main(String[] args) {
        @MySecrecyAnnotations.Secrecy(MySecrecy.L)
        String high = "High";
        @MySecrecyAnnotations.Secrecy(MySecrecy.L)
        String low;

        @MySecrecyAnnotations.Secrecy(MySecrecy.L)
            boolean cond1 = true;
        @MySecrecyAnnotations.Secrecy(MySecrecy.H)
            boolean cond2 = false;

        if (cond1 || cond2) {
            low = high;
        }
    }
}

