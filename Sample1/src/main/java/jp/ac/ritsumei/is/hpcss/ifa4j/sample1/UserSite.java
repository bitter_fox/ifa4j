/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.sample1;

import static jp.ac.ritsumei.is.hpcss.ifa4j.sample1.MySecrecy.*;
import jp.ac.ritsumei.is.hpcss.ifa4j.sample1.MySecrecyAnnotations.*;

/**
 *
 * @author bitter_fox
 */
public class UserSite {
    private class Password {
        @Secrecy(H)
        private String password;
        
        public @Secrecy(L) boolean check(@Secrecy(H) String password) {
            return (boolean)
                    this.password.equals((String)password);
        }
        
        @HeapEffect(H)
        public void setPassword(@Secrecy(H) String password) {
            this.password = password;
        }
    }

    public static void main(/* <nullsecrecy> */String[] args) {
        @Secrecy(H) Password password = null;
        @Secrecy(L) boolean loggedIn = password.check("hoge");
        System.err.println(loggedIn);
        // <any> = <nullsecrecy>; ok
        // <nullsecrecy> = <any>; ok <nullsecrecy> is treated as bottom of <any> secrecy
        // <any S1> = <any S2>; it's ok when S1 :> S2, otherwise it's compile-time error
        // (<any S1> Type)<any S2>; always ok
        // (<nullsecrecy> Type)<any>; always ok
    }
}
