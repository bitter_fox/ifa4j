/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.sample1;

import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyLattice;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.Top;

/**
 *
 * @author bitter_fox
 */
public class SecrecyAsNested {
    @SecrecyLattice
    public enum NestedSecrecy {
        @Top H;
    }
}
