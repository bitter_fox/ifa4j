/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.sample1;

import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyLattice;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.Top;
import jp.ac.ritsumei.is.hpcss.ifa4j.sample1.MySecrecyAnnotations.LowerOf;

/**
 *
 * @author bitter_fox
 */
@SecrecyLattice(name="MySecrecy")
public enum MySecrecy {
    @Top H, // @Highest?
        @LowerOf(H) MM,
        @LowerOf(H) M,
            @LowerOf(M) L,
    @LowerOf({MM, L}) ML,
    @LowerOf(ML) LL,
    ;
}
