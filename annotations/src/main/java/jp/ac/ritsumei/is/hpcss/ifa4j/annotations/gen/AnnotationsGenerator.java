/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.annotations.gen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyAnnotationClass;
import jp.ac.ritsumei.is.hpcss.ifa4j.annotations.SecrecyLattice;

/**
 *
 * @author bitter_fox
 */
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({
    "SecrecyLattice"
})
public class AnnotationsGenerator extends AbstractProcessor {
    int n;
    
    @Override
    public boolean process(
            Set<? extends TypeElement> annotations,
            RoundEnvironment roundEnv) {
        System.err.println(this.getClass()+""+n++);
//        System.err.println(LocalDateTime.now());
//        System.err.println(annotations);
        List<String> generatedName = new ArrayList<>();

        Filer filer = processingEnv.getFiler();
        Messager messager = processingEnv.getMessager();
        Elements elements = processingEnv.getElementUtils();
        
        List<TypeElement> secrecyDefines = roundEnv.getElementsAnnotatedWith(SecrecyLattice.class).stream()
                .filter(e -> e.getKind().isClass())
                .map(e -> (TypeElement)e)
                .collect(Collectors.toList());
        for (TypeElement root : secrecyDefines) {
            String enumFullName = root.getQualifiedName().toString();
            String enumClassName = root.getSimpleName().toString();
            SecrecyLattice sd = root.getAnnotation(SecrecyLattice.class);
            String secrecyName = sd.name().isEmpty()
                    ? enumClassName
                    : sd.name();
            String nameOfAnnotationClass = sd.nameOfAnnotationClass().isEmpty()
                    ? secrecyName + "Annotations"
                    : sd.nameOfAnnotationClass();

            PackageElement p = elements.getPackageOf(root);
            String packageName = p.getQualifiedName().toString();
            String backendFqn = p.isUnnamed()
                    ? nameOfAnnotationClass
                    : packageName+"."+nameOfAnnotationClass;
            TypeElement te = elements.getTypeElement(backendFqn);
            if (te != null) { // Backend class is already exist
                SecrecyAnnotationClass sb = te.getAnnotation(SecrecyAnnotationClass.class);
                if (sb == null // But this is not SecrecyAnnotationClass
                        || !sb.className().equals(enumFullName)) { // or SecrecyAnnotationClass for other SecrecyLattice
                    messager.printMessage(Diagnostic.Kind.ERROR, "Duplicated!!", root);
                    continue;
                }
                // otherwise; update Backend
            } else if (generatedName.contains(backendFqn)) { // already generated in this process
                    messager.printMessage(Diagnostic.Kind.ERROR, "Duplicated!!", root);
                    continue;
            }

            JavaFileObject backendFile;
            try {
                backendFile = filer.createSourceFile(backendFqn);
                try (Writer w = backendFile.openWriter()) {
                    generateAnnotationClass(w, packageName, enumFullName, nameOfAnnotationClass);
                    generatedName.add(backendFqn);
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                Logger.getLogger(AnnotationsGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
            //        System.err.println("END!!!!");
        return !generatedName.isEmpty();
    }
    
    private void generateAnnotationClass(Writer out, String packageName, String enumFullName, String nameOfBackend) throws IOException {
//        Map<String, String> backendModel = new HashMap<>();
//        backendModel.put("packageName", packageName.isEmpty() ? null : packageName);
//        backendModel.put("enumFullName", enumFullName);
//        backendModel.put("nameOfAnnotationClass", nameOfBackend);
//
//        MustacheFactory mf = new DefaultMustacheFactory();
//        Mustache backendTemplate = mf.compile("backendTemplate.mustache");
//        backendTemplate.execute(out, backendModel);
        String packageDeclaration = packageName == null || packageName.isEmpty() ? "" : "package " + packageName + ";";
        String template;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("template.temp")))) {
            template = br.lines()
//                    .peek(System.out::println)
//                    .peek(t -> System.out.println(MessageFormat.format(t, packageDeclaration, enumFullName, nameOfBackend)))
                    .collect(Collectors.joining("\n"));
        }
//        System.out.println(template);
//        System.out.println(MessageFormat.format(template, packageDeclaration, enumFullName, nameOfBackend));
        out.write(MessageFormat.format(template, packageDeclaration, enumFullName, nameOfBackend));
        out.flush();
    }
}
