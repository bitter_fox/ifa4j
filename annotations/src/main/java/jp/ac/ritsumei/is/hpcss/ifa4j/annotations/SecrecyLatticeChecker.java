/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.ac.ritsumei.is.hpcss.ifa4j.annotations;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.SimpleAnnotationValueVisitor8;
import javax.tools.Diagnostic;

/**
 *
 * @author bitter_fox
 */
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({
    "SecrecyAnnotationClass"
})
public class SecrecyLatticeChecker extends AbstractProcessor {
    public Messager messager;
    int n;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        messager = processingEnv.getMessager();
//        System.err.println(this.getClass()+":"+n++);
//        System.err.println(annotations);
//        System.err.println(roundEnv.getElementsAnnotatedWith(SecrecyAnnotationClass.class));
        for (Element root : roundEnv.getElementsAnnotatedWith(SecrecyAnnotationClass.class)) {
            check(roundEnv, root);
        }
        return true;
    }

    public void check(RoundEnvironment roundEnv, Element backendRoot) {
        String name = backendRoot.getAnnotation(SecrecyAnnotationClass.class).className();
        TypeElement secrecyDefine = processingEnv.getElementUtils().getTypeElement(name);
//        roundEnv.getElementsAnnotatedWith(SecrecyLattice.class).forEach(System.err::println);
        AnnotationMirror secrecyDefineAnnotation = findAnnotationMirror(secrecyDefine, SecrecyLattice.class);
        
        Element lowerOf = backendRoot.getEnclosedElements().stream()
                .filter(e -> isAnnotatedBy(e, LowerOfAnnotation.class))
                .findFirst().get();

        if (!checkRootIsEnum(secrecyDefine, secrecyDefineAnnotation)) return;
        if (!checkAnnotationsForOnlyEnumConstants(secrecyDefine.getEnclosedElements(), lowerOf)) return;

        List<Element> enumConstants = secrecyDefine.getEnclosedElements().stream()
                .filter(e -> e.getKind() == ElementKind.ENUM_CONSTANT)
                .collect(Collectors.<Element>toList());
        if (!checkEnumConstantsMoreOne(secrecyDefine, enumConstants)) return;
        if (!checkAnnotatedFirstEnumConstantByTop(enumConstants)) return;
        if (!checkAnnotatedRestEnumConstantsByLowerOf(enumConstants, lowerOf)) return;
        checkLattice(enumConstants, lowerOf, secrecyDefine, secrecyDefineAnnotation);
    }
    
    public boolean checkRootIsEnum(Element root, AnnotationMirror secrecyDefineAnnotation) {
        if (root.getKind() != ElementKind.ENUM) {
            messager.printMessage(Diagnostic.Kind.ERROR, "SecrecyDefine must annotate ENUM", root, secrecyDefineAnnotation);
            return false;
        }
        return true;
    }
    
    public boolean checkAnnotationsForOnlyEnumConstants(List<? extends Element> members, Element lowerOf) {
        boolean success = true;
        for (Element e : members) {
            AnnotationMirror anno = findAnnotationMirror(e, lowerOf);
            if (anno != null) {
                if (e.getKind() != ElementKind.ENUM_CONSTANT) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "LowerOf must annnotate ENUM_CONSTANTES", e, anno);
                    success = false;
                }
            }
        }
        return success;
    }
    
    public boolean checkEnumConstantsMoreOne(Element root, List<Element> enumConsts) {
        if (enumConsts.isEmpty()) {
            messager.printMessage(Diagnostic.Kind.ERROR, "SecrecyDefine enum needs one or more elements", root);
            return false;
        }
        return true;
    }
    
    public boolean checkAnnotatedFirstEnumConstantByTop(List<Element> enumConsts) {
        boolean success = true;
        
        Element first = enumConsts.get(0);
        // TODO: Does it need @Top for First element?
        if (first.getAnnotation(Top.class) == null) {
            messager.printMessage(Diagnostic.Kind.ERROR, "First element of SecrecyDefine enum must be annotated by @Top", first);
            success = false;
        }
        
        List<Element> tops = enumConsts.stream()
                .filter(e -> isAnnotatedBy(e, Top.class))
                .collect(Collectors.toList());
        if (tops.size() > 1) {
            tops.stream()
                    .skip(1)
                    .forEach(top ->
                    messager.printMessage(Diagnostic.Kind.ERROR,
                            "Multiple elements annotated by @Top are found. The element annotated by @Top must be only first one.",
                            top, findAnnotationMirror(top, Top.class)));
            success = false;
        }
        
        return success;
    }
    
    public boolean checkAnnotatedRestEnumConstantsByLowerOf(List<Element> enumConsts, Element lowerOf) {
        boolean[] success = {true};
        
        Element first = enumConsts.get(0);
        AnnotationMirror anno = findAnnotationMirror(first, lowerOf);
        if (anno != null) {
            messager.printMessage(Diagnostic.Kind.ERROR,
                    "Cannot annotate the first element by @LowerOf", first, anno);
            success[0] = false;
        }
        
        enumConsts.stream()
                .skip(1)
                .filter(e -> !isAnnotatedBy(e, lowerOf))
                .peek(e -> success[0] = false)
                .forEach(e -> 
                        messager.printMessage(Diagnostic.Kind.ERROR,
                                "The rest of elements must be annotated by @LowerOf", e));
        
        return success[0];
    }
    
    public boolean checkLattice(List<Element> enumConsts, Element lowerOf, Element root, AnnotationMirror secrecyDefineAnnotation) {
        // TODO: Latticeの定義で誤解をしていたので，改めて実装し直す必要がある．
        
        List<Element> bottomCandidates = new ArrayList<>(enumConsts);
        ExecutableElement valueElement = (ExecutableElement)lowerOf.getEnclosedElements().stream()
                            .filter(el -> el.getSimpleName().contentEquals("value"))
                            .findFirst().get();

        for (Element e : enumConsts) {
            AnnotationMirror lowerOfAnno = findAnnotationMirror(e, lowerOf);
            if (lowerOfAnno == null) {
                continue;
            }

            AnnotationValue value = lowerOfAnno.getElementValues().get(valueElement);

            value.accept(new SimpleAnnotationValueVisitor8<Void, Void>() {
                @Override
                public Void visitArray(List<? extends AnnotationValue> vals, Void p) {
//                    System.err.println(vals);
                    vals.forEach(v -> v.accept(this, p));
                    return null;
                }

                @Override
                public Void visitEnumConstant(VariableElement c, Void p) {
//                    System.err.print(bottomCandidates + "-" + c + ":");
                    bottomCandidates.remove(c);
//                    System.err.println(bottomCandidates);
                    return null;
                }
            }, null);
        }

//        System.err.println(bottomCandidates);
        if (bottomCandidates.size() > 1) {
                messager.printMessage(Diagnostic.Kind.ERROR,
                        "Multiple bottoms are found: " + bottomCandidates + System.lineSeparator() +
                                "Secrecy must be lattice(Bottom must be one)", root, secrecyDefineAnnotation);
            return false;
        }
        return true;
    }

    public boolean isAnnotatedBy(Element e, Class<? extends Annotation> anno) {
        return e.getAnnotation(anno) != null;
    }
    
    public AnnotationMirror findAnnotationMirror(Element e, Class<? extends Annotation> anno) {
        Element annoElement = processingEnv.getElementUtils().getTypeElement(anno.getCanonicalName());
        return findAnnotationMirror(e, annoElement);
    }
    
    public boolean isAnnotatedBy(Element e, Element anno) {
        return findAnnotationMirror(e, anno) != null;
    }
    
    public AnnotationMirror findAnnotationMirror(Element e, Element anno) {
        for (AnnotationMirror annoMirror : e.getAnnotationMirrors()) {
            if (processingEnv.getTypeUtils().isSameType(annoMirror.getAnnotationType(), anno.asType())) {
                return annoMirror;
            }
        }
        return null;
    }
}
